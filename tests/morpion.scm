(use-modules (greenshadow)
             (greenshadow tiles)
             (srfi srfi-1)
             (ice-9 match))

(define running? #t)
(define mkid 0)
(define board #f)
(define mytiles #f)
(define bg-anim (list #t #t #t))
(define bg-color '(0 100 200 255))
(define play-ai #f)

(define (next-bg-color)
  (define (next-value c x)
    (let* ((up? (list-ref bg-anim x))
           (n (if up? (+ c 1) (- c 1))))
      (cond
       ((< n 0)
        (list-set! bg-anim x #t)
        0)
       ((> n 255)
        (list-set! bg-anim x #f)
        255)
       (else n))))
  (match bg-color
    ((r g b _)
     (let ((color (list (next-value r 0)
                        (next-value g 1)
                        (next-value b 2)
                        255)))
       (set! bg-color color)
       bg-color))) )

(define (ai)
  (define (get-pos)
    (let* ((x (random 3))
           (y (random 3))
           (cur (array-ref board x y)))
      (if (= cur -1)
          (list x y)
          (get-pos))))
  (define (play)
    (match (get-pos)
      ((x y)
       (add-element mytiles
                    (make-element mkid
                                  (if (zero? (modulo mkid 2))
                                      'cross 'cercle)
                                  (list x y)))
       (set! mkid (1+ mkid))
       (array-set! board 0 x y))))
  (let ((free-space (member #t
                            (map list?
                                 (map (lambda (l)
                                        (member -1 l))
                                      (array->list board))))))
    (if free-space
        (play)
        (set! running? #f))))

(define (make-new-game)
  (set! mkid 0)
  (set! board (make-array -1 3 3))
  (set! mytiles
    (make-tiles 50 50 380 380 3 3
                #:elements (list)
                #:grid? #f
                #:bg-color '(200 200 200 255))))

(define (load)
  (load-sprite 'cross "data/cross.png")
  (load-sprite 'cercle "data/cercle.png")
  (make-new-game))

(define (ok? l x)
  (and (not (member -1 l))
       (or (= x 3) (= x 0))))

(define (is-game-over?)
  (let* ((lines (array->list board))
         (columns (map (lambda (y)
                         (map (lambda (x)
                                (array-ref board x y))
                              '(0 1 2)))
                       '(0 1 2)))
         (diag1 (map (lambda (x) (array-ref board x x))
                     '(0 1 2)))
         (diag2 (map (lambda (x) (array-ref board x (- 2 x)))
                     '(0 1 2)))
         (lines-sum   (map (lambda (x) (apply + x)) lines))
         (columns-sum (map (lambda (x) (apply + x)) columns)))
    (or (not (null? (remove not (map ok? lines lines-sum))))
        (not (null? (remove not (map ok? columns columns-sum))))
        (ok? diag1 (apply + diag1))
        (ok? diag2 (apply + diag2)))))

(define (update)
  (when (and running?
             (is-game-over?))
    (set! running? #f))
  (when (and running?
             play-ai)
    (ai)
    (set! play-ai #f)))

(define (draw)
  (apply set-draw-color (next-bg-color))
  (draw-rect 0 0 480 480 #:fill? #t)
  (draw-tiles mytiles)
  (unless running?
    (draw-text "GAME OVER !"
               #:pos '(150 200)
               #:color '(250 20 20 255))
    (draw-text "[Click to start new game]"
               #:pos '(80 220)
               #:color '(100 0 250 255))))

(define (key pressed? key repeat? mod)
  (not (and (not pressed?)
            (equal? key 'escape))))

(define (mouse pressed? button x y)
  (unless running?
    (when (not pressed?)
      (make-new-game)
      (set! running? #t)))
  (when running?
    (unless pressed?
      (let ((pos (is-on-tiles mytiles x y)))
        (when (and pos (not (get-element-by-pos mytiles pos)))
          (add-element mytiles
                       (make-element mkid
                                     (if (zero? (modulo mkid 2))
                                         'cross 'cercle)
                                     pos))
          (set! mkid (1+ mkid))
          (array-set! board (modulo mkid 2)
                      (car pos) (cadr pos))
          (set! play-ai #t))))))

(run #:title "Greenshadow -- Morpion"
     #:size '(480 480)
     #:load load #:update update #:draw draw
     #:key-event key #:mouse-event mouse
     #:background-color '(0 0 0 255))
