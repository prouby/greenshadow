(use-modules (greenshadow)
             (greenshadow tiles))

(define (load)
  (load-sprite 'cookies "cookies.png")
  (data-store 'mytiles
              (make-tiles 50 50 380 380 4 4
                          #:elements (list (make-element 0 'cookies (list 0 0))
                                           (make-element 1 'cookies (list 1 1))
                                           (make-element 2 'cookies (list 2 2))
                                           (make-element 3 'cookies (list 3 3)))
                          #:grid? #t
                          #:grid-color '(255 255 255 255)
                          #:bg-color '(140 0 165 255))))

(define (update) #t)
(define (draw)
  (draw-text "Presse ESCAPE to exit")
  (draw-tiles (data-get 'mytiles)))

(define (key pressed? key repeat? mod)
  "Return false when KEY is 'escape' and is realised."
  (not (and (not pressed?)
            (equal? key 'escape))))

(define (mouse pressed? button x y)
  "Return false when KEY is 'escape' and is realised."
  (display (is-on-tiles (data-get 'mytiles) x y))
  (newline))


(run #:title "TEST - Tiles"
     #:size '(480 480)
     #:load load #:update update
     #:draw draw
     #:key-event key
     #:mouse-event mouse)
