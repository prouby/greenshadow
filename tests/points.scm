(use-modules (greenshadow))

(define points-x (iota 480))
(define points-y (iota 480))

;;; Create line with list of points
(define h-line-points
  (map (λ (x)
         (list x 240))
       points-x))

;;; Void functions
(define (load) #t)
(define (update) #t)

;;; Draw function
(define (draw)
  (draw-text "Presse ESCAPE to exit")

  (set-draw-color 255 255 255 255)
  (map (λ (x y) (draw-point x y)) points-x points-y)

  (set-draw-color 255 0 0 255)
  (map (λ (x y) (draw-point x y)) points-x (reverse points-y))

  (set-draw-color 0 0 255 255)
  (draw-points h-line-points))

;;; Key handler
(define (key pressed? key repeat? mod)
  "Return false when KEY is 'escape' and is realised."
  (not (and (not pressed?)
            (equal? key 'escape))))

;;; Run the game
(run #:title "TEST - Points"
     #:size '(480 480)
     #:load load #:update update
     #:draw draw
     #:key-event key)
