(use-modules (greenshadow))

(run #:controller-button-event
     (lambda (pressed? button)
       (format #t "~a ~a\n" button
               (if pressed? "Pressed" "Realesed")))
     #:controller-axis-event
     (lambda (axis value)
       (format #t "~a ~a\n" axis value)))
