(use-modules (greenshadow))

(define x 150)
(define y 150)

(define x2 100)
(define y2 200)

;;; Void functions
(define (load) #t)
(define (update)
  (set! x (modulo (+ x 1) 480))
  (set! y (modulo (- y 1) 480))

  (set! x2 (modulo (+ x2 1) 480))
  (set! y2 (modulo (+ y2 1) 480)))

;;; Draw function
(define (draw)
  (draw-text "Presse ESCAPE to exit")

  (set-draw-color 255 0 0 255)
  (draw-rect x y 150 150 #:fill? #t)

  (set-draw-color 0 0 255 255)
  (draw-rects (list (list x2 y2 150 150)
                    (list 200 200 40 40))))

;;; Key handler
(define (key pressed? key repeat? mod)
  "Return false when KEY is 'escape' and is realised."
  (not (and (not pressed?)
            (equal? key 'escape))))

;;; Run the game
(run #:title "TEST - Rect"
     #:size '(480 480)
     #:load load #:update update
     #:draw draw
     #:key-event key)
