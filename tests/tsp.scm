;;; https://en.wikipedia.org/wiki/Travelling_salesman_problem

(use-modules (greenshadow)
             (ice-9 match)
             (srfi srfi-1))

(define NB-FRAME-UPDATE 1)
(define NB-POINTS 500)
(define WIDTH 640)
(define HEIGHT 480)

(define frames 0)

(define (dist p1 p2)
  (match (list p1 p2)
    (((_ x1 y1) (_ x2 y2))
     (let ((x (- x2 x1))
           (y (- y2 y1)))
       (sqrt (+ (* x x) (* y y)))))))

(define (fmin old new)
  (if (not old) #t
      (< (car new) (car old))))

(define (for-each->min-ind p lst)
  (define (aux l acc)
    (match l
      (() acc)
      ((x . r)
       (let ((lesser? (p acc x)))
         (aux r (if lesser? x acc))))))
  (aux lst #f))

(define (load)
  (set! *random-state* (seed->random-state (current-time)))

  (let* ((np (iota NB-POINTS))
         (lst-p (map (lambda (x)
                       `(,x . ,(list (random (- WIDTH 5))
                                     (random (- HEIGHT 5)))))
                     np)))
    (data-store 'lst-lines '())
    (data-store 'lst-points-draw lst-p)
    (data-store 'lst-points (cdr lst-p))
    (data-store 'first-point (car lst-p))
    (data-store 'current-point (car lst-p))))

(define (greedy lst)
  (let* ((cur (data-get 'current-point))
         (dists (map (lambda (point)
                       (let ((d (dist cur point)))
                         `(,d . ,point)))
                     lst))
         (dmin (for-each->min-ind fmin dists)))
    (data-store 'lst-lines (cons (append (map (lambda (x) (+ 2 x))
                                              (cdr cur))
                                         (map (lambda (x) (+ 2 x))
                                              (cddr dmin)))
                                 (data-get 'lst-lines)))
    (data-store 'lst-points (remove (lambda (x)
                                      (equal? x (cdr dmin)))
                                    (data-get 'lst-points)))
    (data-store 'current-point (cdr dmin))))

(define (update)
  (when (= frames NB-FRAME-UPDATE)
    (set! frames 0)

    (let ((lst (data-get 'lst-points)))
      (unless (null-list? lst)
        (greedy lst)))))

(define (draw)
  (set-draw-color 255 255 255 255)
  (draw-lines (data-get 'lst-lines))
  (apply draw-line (append (cdr (data-get 'current-point))
                           (cdr (data-get 'first-point))))

  (set-draw-color 0 255 0 255)
  (draw-rects (map (lambda (l) (append (cdr l) '(5 5)))
                   (data-get 'lst-points-draw))
              #:fill? #t)

  (set! frames (1+ frames)))

(define (key pressed? key repeat? modifier)
  (not (or (equal? key 'q)
           (equal? key 'escape))))

(run #:title (format #f "TSP - Greedy - (~ax~a) - ~a points"
                     WIDTH HEIGHT NB-POINTS)
     #:size (list WIDTH HEIGHT)
     #:load load
     #:update update
     #:draw draw
     #:key-event key
     #:background-color '(12 12 12 255)
     #:user-ht-size 10
     #:texture-ht-size 0
     #:sound-ht-size 0
     #:font-ht-size 0)
