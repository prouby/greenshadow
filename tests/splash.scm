(use-modules (greenshadow))

;;; Run demo
(run #:load (lambda () (sleep 5))
     #:draw (lambda () (draw-text "Ok !"))
     #:background-color '(64 64 64 255))
