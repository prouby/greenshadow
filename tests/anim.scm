(use-modules (greenshadow)
             (greenshadow animation)
             (srfi srfi-1)
             (ice-9 match))

(define dead? #f)

(define (load-path s)
  (format #f "data/freedoom/~a" s))

(define (load)
  (load-sprite 'head (load-path "heada1.png"))
  (for-each (lambda (x)
              (match x
                ((k v)
                 (load-sprite k (load-path v)))))
            '((a "headg0.png")
              (b "headh0.png")
              (c "headi0.png")
              (d "headj0.png")
              (e "headk0.png")
              (f "headl0.png")))
  (data-store 'death-anim (make-animation (/ 60 4)
                                          (map (lambda (x)
                                                 (list x '(110 100) 4 0))
                                               '(a b c d e f))))
  (data-store 'freedoom
              (make-animation (/ 60 4)
                              (map (lambda (x)
                                     (lambda ()
                                       (draw-text "FreeDooM"
                                                  #:pos '(350 440)
                                                  #:color x)))
                                   '((200 0 0 255)
                                     (200 200 0 255)))))
  (data-store 'amazdoom
              (make-animation (/ 60 4)
                              (map (lambda (x)
                                     (lambda ()
                                       (draw-text "AmazDooM"
                                                  #:pos '(350 400)
                                                  #:color x)))
                                   '((200 0 0 255)
                                     (200 200 0 255)))))
  (load-sound 'death (load-path "dssgtdth.wav"))
  (load-music 'main (load-path "main.mid"))
  (set-music-volume 42)
  (play-music 'main #:loop -1))

(define (update)
  #t)

(define (draw)
  (if dead?
      (let ((end? (draw-animation (data-get 'death-anim))))
        (when end?
          (set! dead? #f)))
      (draw-sprite 'head #:pos '(110 100) #:size 4))

  (draw-text "(c) Sprites, musics and sounds:"
             #:pos '(5 440)
             #:color '(200 32 32 255))
  (draw-animation (data-get 'freedoom))

  (draw-text "(c) Font:"
             #:pos '(5 400)
             #:color '(200 32 32 255))
  (draw-animation (data-get 'amazdoom)))

(define (key pressed? key repeat? mod)
  (not (and (not pressed?)
            (equal? key 'escape))))

(define (mouse pressed? button x y)
  (when (and (not pressed?) (not dead?))
    (set! dead? #t)
    (play-sound 'death)))

(run #:title "Greenshadow -- Animation"
     #:size '(480 480)
     #:load load #:update update #:draw draw
     #:mouse-event mouse
     #:font-size 38
     #:font (load-path "AmazDooMLeft.ttf")
     #:key-event key
     #:show-fps? #t
     ;; #:smooth? #t
     #:background-color '(32 32 32 255))
