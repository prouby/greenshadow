(use-modules (greenshadow))

;;; Create list of horizontal lines
(define lines
  (map (λ (y) (list 0 y 480 y))
       (map (λ (y) (* y 10))
            (iota 48))))

(define frames 0)

;;; Void functions
(define (load) #t)
(define (update) #t)

;;; Draw function
(define (draw)
  (set-draw-color 255 255 255 255)
  (draw-line 0 0 480 480)

  (set-draw-color 255 0 0 255)
  (draw-line 0 480 480 0)

  (set-draw-color 0 0 255 128)

  (when (zero? (modulo frames 3))
    (set! lines (map (λ (l)
                       (let ((y (list-ref l 1)))
                         (list 0 (modulo (1+ y) 480)
                               480 (modulo (1+ y) 480))))
                     lines)))

  (draw-lines lines)

  (draw-text "Presse ESCAPE to exit" #:pos '(0 450))
  (set! frames (1+ frames)))

;;; Key handler
(define (key pressed? key repeat? mod)
  "Return false when KEY is 'escape' and is realised."
  (not (and (not pressed?)
            (equal? key 'escape))))

;;; Run the game
(run #:title "TEST - Lines"
     #:size '(480 480)
     #:load load #:update update
     #:draw draw
     #:show-fps? #t
     #:key-event key)
