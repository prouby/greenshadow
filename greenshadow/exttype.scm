;;; greenshadow --- GreenShadow2D game engine
;;
;; Copyright © 2020 by Pierre-Antoine Rouby <contact@parouby.fr>
;;
;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>

(define-module (greenshadow exttype)
  #:use-module (srfi srfi-9)
  #:export (ext-sprite
            make-ext-sprite
            ext-sprite-get-texture
            ext-sprite-get-size))

(define-record-type ext-sprite
  (make-ext-sprite texture size)
  ext-sprite?
  (texture ext-sprite-get-texture)
  (size ext-sprite-get-size))
