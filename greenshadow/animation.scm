;;; greenshadow --- GreenShadow2D game engine
;;
;; Copyright © 2020 by Pierre-Antoine Rouby <contact@parouby.fr>
;;
;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>

(define-module (greenshadow animation)
  #:use-module (greenshadow)
  #:use-module (srfi srfi-9)
  #:use-module (ice-9 match)
  #:export (make-animation
            draw-animation))

(define-record-type animation
  (%make-animation current-frame frame-delay
                   nb draw-list)
  animation?
  (current-frame animation-get-current-frame animation-set-current-frame!)
  (frame-delay   animation-get-frame-delay   animation-set-frame-delay!)
  (nb            animation-get-nb)
  (draw-list     animation-get-draw-list))

(define (make-animation delay draw-list)
  (%make-animation 0 delay (length draw-list) draw-list))

(define (clone-animation anim)
  (%make-animation (animation-get-current-frame anim)
                   (animation-get-frame-delay   anim)
                   (animation-get-nb            anim)
                   (animation-get-draw-list     anim)))

(define* (draw-animation anim)
  (match anim
    (($ animation f d n lst)
     (let* ((frame (modulo (1+ f) (* n d)))
            (ref   (floor (/ frame d)))
            (end?  (= frame 0)))
       (animation-set-current-frame! anim frame)
       (unless end?
         (match (list-ref lst ref)
           ((sprite p)
            (if (procedure? sprite)
                (sprite p)
                (draw-sprite sprite #:pos p)))
           ((sprite p s a)
            (if (procedure? sprite)
                (sprite p s a)
                (draw-sprite sprite #:pos p #:size s #:angle a)))
           (sprite
            (if (procedure? sprite)
                (sprite)
                (draw-sprite sprite)))))
       end?))))
