;;; greenshadow --- GreenShadow2D game engine
;;
;; Copyright © 2020 by Pierre-Antoine Rouby <contact@parouby.fr>
;;
;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>

(define-module (greenshadow tiles)
  #:use-module (greenshadow)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-9)
  #:export (make-element
            add-element
            get-element-by-id
            get-element-by-pos
            set-element-by-id
            set-element-by-pos
            make-tiles
            draw-tiles
            is-on-tiles))

(define-record-type <element>
  (%make-element id key pos)
  element?
  (id get-element-id set-element-id!)
  (key get-element-key set-element-key!)
  (pos get-element-pos set-element-pos!))

(define (make-element id key pos)
  (%make-element id key pos))

(define-record-type <tiles>
  (%make-tiles rect size elements
               grid? grid-color
               background-color)
  tiles?
  (rect get-tiles-rect set-tiles-rect!)
  (size get-tiles-size set-tiles-size!)
  (elements get-tiles-elements set-tiles-elements!)
  (grid? get-tiles-grid?)
  (grid-color get-tiles-grid-color)
  (background-color get-background-color set-background-color!))

(define* (make-tiles x y w h n m #:key
                     (elements '())
                     (grid? #f)
                     (grid-color '(0 0 0 255))
                     (bg-color #f))
  "Make new tiles zone (X Y W H), with N column and M line."
  (%make-tiles `(,x ,y ,w ,h)
               `(,n ,m)
               elements
               grid?
               grid-color
               bg-color))

(define (get-element-by-id tiles id)
  (define (aux l)
    (match l
      (() #f)
      ((x . r)
       (if (equal? (get-element-id x) id)
           x (aux r)))))
  (aux (get-tiles-elements tiles)))

(define (get-element-by-pos tiles pos)
  (define (aux l)
    (match l
      (() #f)
      ((x . r)
       (if (equal? (get-element-pos x) pos)
           x (aux r)))))
  (aux (get-tiles-elements tiles)))


(define (set-element-by-id tiles id new-element)
  (set-tiles-elements!
   tiles
   (map (lambda (x)
          (if (equal? (get-element-id x) id)
              new-element
              x))
        (get-tiles-elements tiles))))

(define (set-element-by-pos tiles pos new-element)
  (set-tiles-elements!
   tiles
   (map (lambda (x)
          (if (equal? (get-element-pos x) pos)
              new-element
              x))
        (get-tiles-elements tiles))))

(define (add-element tiles new-element)
  (set-tiles-elements!
   tiles
   (cons new-element (get-tiles-elements tiles))))

(define (draw-grid rect size color)
  (match `(,rect ,size)
    (((x y w h) (n m))
     (let ((pos-x (iota (+ n 1)))
           (pos-y (iota (+ m 1)))
           (size-x (round (/ w n)))
           (size-y (round (/ h m))))
       (apply set-draw-color color)
       (for-each (lambda (c)
                   (let ((x (+ x (* size-x c))))
                     (draw-line x y x (+ y h))))
                 pos-x)
       (for-each (lambda (l)
                   (let ((y (+ y (* size-y l))))
                     (draw-line x y (+ x w) y)))
                 pos-y)))))

(define (draw-element rect size element)
  (match element
    (($ <element> id key pos)
     (match `(,rect ,size ,pos)
       (((x y w h) (n m) (ex ey))
        (let* ((size-x (round (/ w n)))
               (size-y (round (/ h m)))
               (pos (list (+ x (* ex size-x))
                          (+ y (* ey size-y)))))
          (draw-sprite key #:pos pos
                       #:size (list size-x size-y))))))))

(define (draw-tiles tiles)
  (match tiles
    (($ <tiles> rect size elements grid? grid-color bg-color)
     (when bg-color
       (apply set-draw-color bg-color)
       (apply (lambda (x y w h)
                (draw-rect x y w h #:fill? #t))
              rect))
     (when grid? (draw-grid rect size grid-color))
     (for-each (lambda (e) (draw-element rect size e))
               elements))))

(define (is-on-tiles tiles px py)
  (match tiles
    (($ <tiles> (x y w h) (n m) _ _ _ _)
     (if (and (> px x)
              (> py y)
              (< px (+ x w))
              (< py (+ y h)))
         (let* ((sx (/ w n))
                (sy (/ h m))
                (cx (floor (/ (- px x) sx)))
                (cy (floor (/ (- py y) sy))))
           (list cx cy))
         #f))))
