;;; greenshadow --- GreenShadow2D game engine
;;
;; Copyright © 2020 by Pierre-Antoine Rouby <contact@parouby.fr>
;;
;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>

(define-module (greenshadow demo)
  #:use-module (greenshadow config)
  #:use-module (greenshadow)
  #:use-module (ice-9 match)
  #:export (demo-load
            demo-draw
            demo-update
            demo-mouse
            demo-key
            demo-button
            demo-axis
            demo-atexit))

(define %angle 0)

(define (demo-mouse pressed? button x y) #t)
(define (demo-key pressed? key repeat? modifier) #t)
(define (demo-button pressed? button) #t)
(define (demo-axis axis value) #t)
(define (demo-atexit) (display "Bye !") (newline))

(define (demo-load)
  (load-sprite 'logo (with-default-path "data/green.png"))
  (text->sprite 'text "GreenShadow demo"))

(define (demo-draw)
  (draw-sprite 'logo
               #:pos '(80 0)
               #:size '(480 480)
               #:angle %angle)
  (draw-sprite 'text #:pos '(220 0)))

(define (demo-update)
  (set! %angle (modulo (+ %angle 1) 360)))
