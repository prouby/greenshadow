;;; greenshadow --- GreenShadow2D game engine
;;
;; Copyright © 2020 by Pierre-Antoine Rouby <contact@parouby.fr>
;;
;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>

(define-module (greenshadow)
  #:use-module (greenshadow demo)
  #:use-module (greenshadow config)
  #:use-module (greenshadow exttype)
  #:use-module (sdl2)
  #:use-module ((sdl2 ttf) #:prefix ttf:)
  #:use-module (sdl2 render)
  #:use-module (sdl2 surface)
  #:use-module (sdl2 events)
  #:use-module (sdl2 image)
  #:use-module (sdl2 video)
  #:use-module ((sdl2 mixer) #:prefix mix:)
  #:use-module (sdl2 rect)
  ;; #:use-module (sdl2 hint)
  #:use-module (sdl2 input mouse)
  #:use-module (sdl2 input game-controller)
  #:use-module (ice-9 match)
  #:export (run
            set-draw-color
            draw-point
            draw-points
            draw-line
            draw-lines
            draw-rect
            draw-rects
            ;; Data
            data-store
            data-get
            data-remove
            ;; Sprite
            load-sprite
            free-sprite
            draw-sprite
            ;; Text
            load-font
            free-font
            draw-text
            text->sprite
            ;; Music and sound
            load-music
            free-music
            play-music
            load-sound
            free-sound
            play-sound
            set-music-volume
            set-sound-volume))

(define %texture-ht #f)
(define %sound-ht #f)
(define %font-ht #f)
(define %user-ht #f)

(define %render #f)
(define %window #f)
(define %gs-width #f)
(define %gs-height #f)
(define %gs-font #f)
(define %background-color #f)

(define %frames 0)
(define %frames-cnt 0)
(define %time (current-time))
(define %fps 0)

(define %controllers '())

(define (find-controllers)
  (define (find-controller i acc)
    (let ((c (game-controller-index? i)))
      (cond
       (c (find-controller (- i 1)
                           (cons (open-game-controller i)
                                 acc)))
       ((> i 0) (find-controller (- i 1) acc))
       (else acc))))
  (find-controller 32 '()))

(define (set-window-size! size)
  (match size
    ((or (w h) #(w h))
     (set! %gs-width w)
     (set! %gs-height h))
    (_ (throw 'greenshadow (list "set-window-size!" size)))))

(define (free-memory!)
  ;; Texture
  (hash-for-each
   (lambda (k v)
     (hash-remove! %texture-ht k)
     (match v
       (($ ext-sprite tex _)
        (delete-texture! tex))
       (_
        (delete-texture! v))))
   %texture-ht)
  ;; Font
  (hash-for-each
   (lambda (k v)
     (ttf:delete-font! v)
     (hash-remove! %font-ht k))
   %font-ht)
  ;; Music and sound
  (hash-for-each
   (lambda (k v)
     (hash-remove! %sound-ht k)
     (cond
      ((mix:chunk? v) (mix:delete-chunk! v))
      (else (mix:delete-music! v))))
   %sound-ht))

(define (render-clear)
  (apply set-render-draw-color (cons %render %background-color))
  (clear-renderer %render))

(define (render-update)
  (present-renderer %render))

(define (gs-main-loop update draw key mouse
                      controller-button controller-axis
                      show-fps?)
  (define (get-events)
    (let ((event (poll-event)))
      (cond
       ((quit-event? event) #f)
       ((keyboard-event? event)
        (and (key (keyboard-event-pressed? event)
                  (keyboard-event-key event)
                  (keyboard-event-repeat? event)
                  (keyboard-event-modifiers event))
             (get-events)))
       ((mouse-button-event? event)
        (and (mouse (mouse-button-event-pressed? event)
                    (mouse-button-event-button event)
                    (mouse-button-event-x event)
                    (mouse-button-event-y event))
             (get-events)))
       ((controller-button-event? event)
        (and (controller-button (controller-button-event-pressed? event)
                                (controller-button-event-button event))
             (get-events)))
       ((controller-axis-event? event)
        (and (controller-axis (controller-axis-event-axis event)
                              (controller-axis-event-value event))
             (get-events)))
       ((not event) #t)
       (else (get-events)))))

  (define (fps!)
    (set! %frames (1+ %frames))
    (set! %frames-cnt (1+ %frames-cnt))
    (when (not (= %time (current-time)))
      (set! %fps %frames-cnt)
      (set! %frames-cnt 0)
      (set! %time (current-time))))

  (define (loop)
    (fps!)
    (render-clear)

    (when (get-events)
      (when update (update))
      (draw)

      (when show-fps?
        (draw-text (format #f "~a fps" %fps)
                   #:pos '(5 5) #:color '(0 255 0 255)))
      (render-update)
      (loop)))

  (loop))


;;;;;;;;;;;;;;;;;;
;; ** PUBLIC ** ;;
;;;;;;;;;;;;;;;;;;


(define (set-draw-color r g b a)
  "Set the draw color."
  (set-render-draw-color %render r g b a))

(define (draw-point x y)
  "Draw point."
  (render-draw-point %render x y))

(define (draw-points points)
  "Draw points."
  (match points
    (() #t)
    ((or ((x y) . r) (#(x y) . r))
     (render-draw-point %render x y)
     (draw-points r))
    (_ (throw 'greenshadow (list "draw-points" points)))))

(define (draw-line x1 y1 x2 y2)
  "Draw line."
  (render-draw-line %render x1 y1 x2 y2))

(define (draw-lines points)
  "Draw multiple lines. POINTS should be a list of points, or a list
of lines."
  (match points
    (() #t)
    ;; List of points
    ((or ((x1 y1) (x2 y2) . r)
         (#(x1 y1) #(x2 y2) . r))
     (render-draw-line %render x1 y1 x2 y2)
     (draw-lines r))
    ;; List of lines
    ((or ((x1 y1 x2 y2) . r)
         (((x1 y1) (x2 y2)) . r)
         ((#(x1 y1 x2 y2)) . r)
         ((#(x1 y1) #(x2 y2)) . r))
     (render-draw-line %render x1 y1 x2 y2)
     (draw-lines r))
    (_ (throw 'greenshadow (list "draw-lines" points)))))

(define* (draw-rect x y w h #:key (fill? #f))
  ;; TODO: Use sdl2 rect fill and draw functions.
  (if fill?
      (render-fill-rect %render (make-rect x y w h))
      (render-draw-rect %render (make-rect x y w h))))

(define* (draw-rects rects #:key (fill? #f))
  (match rects
    (() #t)
    ((or ((x y h w) . r)
         (#(x y h w) . r))
     (draw-rect x y w h #:fill? fill?)
     (draw-rects r #:fill? fill?))
    (_ (throw 'greenshadow (list "draw-rects" rects)))))

;;;;;;;;;;;;
;; SPRITE ;;
;;;;;;;;;;;;

(define (load-sprite key path)
  "Load sprite from PATH."
  (let* ((surface (load-image path))
         (texture (surface->texture %render surface))
         (size    (list (surface-width surface)
                        (surface-height surface))))
    (delete-surface! surface)
    (hash-set! %texture-ht key (make-ext-sprite texture size))))

(define (free-sprite key)
  "Free sprite memory from KEY."
  (let ((sprite (hash-ref %texture-ht key)))
    (when sprite
      (hash-remove! %texture-ht key)
      (match sprite
        (($ ext-sprite texture _)
         (delete-texture! texture))
        (_
         (delete-texture! sprite))))))

(define* (draw-sprite key #:key
                      (pos `(0 0))
                      (size #f)
                      (angle 0))
  "Draw SPRITE at position POS."
  (define dfl-size `(,%gs-width ,%gs-height))
  (define (get-size-or s)
    (match size
      ((_ _) size)
      (#f    s)
      (num   (map (lambda (x) (* x num)) s))
      (else  s)))

  (let ((sprite (hash-ref %texture-ht key)))
    (match sprite
      (($ ext-sprite texture s)
       (render-copy %render texture
                    #:dstrect (append pos (get-size-or s))
                    #:angle angle))
      (texture?
       (render-copy %render sprite
                    #:dstrect (append pos (get-size-or dfl-size))
                    #:angle angle))
      (_ (throw 'greenshadow (list "draw-sprite: No sprite for key." key))))))

;;;;;;;;;;
;; FONT ;;
;;;;;;;;;;

(define (load-font key path size)
  "Load PATH with SIZE."
  (let ((font (ttf:load-font path size)))
    (hash-set! %font-ht key font)))

(define (free-font key)
  "Free font memory from PATH."
  (let ((font (hash-ref %font-ht key)))
    (when font
      (ttf:delete-font! font)
      (hash-remove! %font-ht key))))

(define* (text->sprite key text #:key
                       (color '(255 255 255 255))
                       (font %gs-font))
  "Text to sprite."
  (let* ((surface (ttf:render-font-solid font text
                                         (apply make-color color)))
         (texture (surface->texture %render surface)))
    (delete-surface! surface)
    (hash-set! %texture-ht key
               (make-ext-sprite texture
                                (list (surface-width surface)
                                      (surface-height surface))))))

(define* (draw-text text #:key
                    (color '(255 255 255 255))
                    (font %gs-font)
                    (pos '(0 0)))
  "Draw text at position POS."
  (match pos
    ((x y)
     (let* ((surface (ttf:render-font-solid font text
                                            (apply make-color color)))
            (texture (surface->texture %render surface)))
       (render-copy %render texture
                    #:dstrect
                    (list x y
                          (surface-width surface)
                          (surface-height surface)))
       (delete-texture! texture)
       (delete-surface! surface)))))

;;;;;;;;;;;
;; MUSIC ;;
;;;;;;;;;;;

(define (load-music key path)
  (let ((music (mix:load-music path)))
    (hash-set! %sound-ht key music)))

(define (free-music key)
  (let ((music (hash-ref %sound-ht key)))
    (when music
      (mix:delete-music! music)
      (hash-remove! %sound-ht key))))

(define* (play-music key #:key (loop 1))
  (let ((music (hash-ref %sound-ht key)))
    (mix:play-music! music loop)))

(define (set-music-volume volume)
  (mix:set-music-volume! volume))

;;;;;;;;;;;
;; SOUND ;;
;;;;;;;;;;;

(define* (load-sound key path #:key (volume 128))
  (let ((sound (mix:load-chunk path)))
    (mix:set-chunk-volume! sound volume)
    (hash-set! %sound-ht key sound)))

(define (free-sound key)
  (let ((sound (hash-ref %sound-ht key)))
    (when sound
      (mix:delete-chunk! sound)
      (hash-remove! %sound-ht key))))

(define* (play-sound key #:key (loops 0))
  (let ((sound (hash-ref %sound-ht key)))
    (mix:play-chunk! sound #:loops loops)))

(define (set-sound-volume key volume)
  (let ((sound (hash-ref %sound-ht key)))
    (when (and sound (mix:chunk? sound))
      (mix:set-chunk-volume! sound volume))))

;;;;;;;;;;
;; DATA ;;
;;;;;;;;;;

(define (data-store key object)
  (hash-set! %user-ht key object))

(define (data-get key)
  (hash-ref %user-ht key))

(define* (data-remove key #:key (free-procedure #f))
  (when free-procedure
    (free-procedure (hash-ref %user-ht key)))
  (hash-remove! %user-ht key))

;;;;;;;;;
;; RUN ;;
;;;;;;;;;

(define* (run #:key
              ;; Config
              (size '(640 480))
              (title "GreenShadow")
              (font (with-default-path "data/default.ttf"))
              (font-size 32)
              (background-color '(0 0 0 255))
              (texture-ht-size 255)
              (sound-ht-size 255)
              (font-ht-size 15)
              (user-ht-size 255)
              (renderer-flags '(accelerated vsync))
              (mixer-flags '(ogg mp3))
              (show-fps? #f)
              (smooth? #t)
              ;; Game callback functions
              (load demo-load)
              (update demo-update)
              (draw demo-draw)
              (mouse-event demo-mouse)
              (key-event demo-key)
              (controller-button-event demo-button)
              (controller-axis-event demo-axis)
              (atexit demo-atexit))

  (define (init-ht)
    (set! %texture-ht (make-hash-table texture-ht-size))
    (set! %sound-ht (make-hash-table sound-ht-size))
    (set! %font-ht (make-hash-table font-ht-size))
    (set! %user-ht (make-hash-table user-ht-size)))

  (define (splash-screen)
    (render-clear)
    (draw-text "Loading ..."
               #:pos (list 5 (- %gs-height (+ font-size 5)))
               #:color (match background-color
                         ((r g b _)
                          (list (- 255 r)
                                (- 255 g)
                                (- 255 b)
                                255))))
    (render-update))

  (sdl-init)
  (ttf:ttf-init)
  (mix:mixer-init mixer-flags)

  (init-ht)
  (set! %gs-font (load-font 'gs-dfl-font font font-size))
  (set! %background-color background-color)
  (set! %controllers (find-controllers))
  (set-window-size! size)

  ;; (when smooth?
  ;;   (set-hint! SDL_HINT_RENDER_SCALE_QUALITY "1"))

  (call-with-window
   (make-window #:title title #:size size)
   (lambda (window)
     (set! %window window)
     (call-with-renderer
         (make-renderer %window renderer-flags)
      (lambda (render)
        (set! %render render)
        (mix:open-audio)

        (splash-screen)
        (when load (load))

        (gs-main-loop update draw
                      key-event
                      mouse-event
                      controller-button-event
                      controller-axis-event
                      show-fps?)

        (atexit)
        (free-memory!)

        (mix:close-audio)
        (mix:mixer-quit)
        (ttf:ttf-quit)
        (sdl-quit))))))
